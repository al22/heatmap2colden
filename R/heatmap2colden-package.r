#' heatmap2colden
#'
#' @name heatmap2colden
#' @docType package
NULL

##' Plot a heatmap with colored row/col dendrograms
##'
##' This function is basically a call to heatmap.2 from the gplots package.
##' It has the added functionality to color branches in the row and/or col
##' dendrograms.
##' @param ... arguments passed through to heatmap.2
##' @param acolNclust number of clusters to color in the columns
##' @param browNclust number of clusters to color in the rows
##' @param ccolCol the colors to use in the column clusters
##' @param drowCol the colors to use in the row clusters
##' @param revcols boolean.  reverse the columns?
##' @param revrows boolean.  reverse the rows?
##' @return the heatmap.2 returned heatmap
##' @author Andreas Leha
##' @name heatmap.2.colden
##' @export
##' @examples
##' data(mtcars)
##' x  <- as.matrix(mtcars)
##' heatmap.2.colden(browNclust=2, x=x)
heatmap.2.colden <- function(...,
                             acolNclust=NULL,
                             browNclust=NULL,
                             ccolCol,
                             drowCol,
                             revcols=NULL,
                             revrows=NULL) {

  ## plot a heatmap only to extract the dendrograms
  tfile <- tempfile()
  png(tfile, width=max(960, dev.size(units="px")[1]), height=max(960, dev.size(units="px")[2]))
  hv <- heatmap.2(...)
  dev.off()
  dev.set(dev.prev())
  unlink(tfile)

  ## normal heatmap.2 if no cluster numbers specified
  if (is.null(acolNclust) && is.null(browNclust)) {
    eval(hv$call)
    return(invisible(hv))
  }

  ## extract the dendrograms
  colDend <- hv$colDendrogram
  rowDend <- hv$rowDendrogram

  addargs <- list(...)
  serRowv <- eval(if ("Rowv" %in% names(addargs)) addargs[["Rowv"]] else TRUE)
  serColv <- eval(if ("Colv" %in% names(addargs)) addargs[["Colv"]] else TRUE)

  ## reverse?
  if (!is.null(revcols) && revcols) {
    colDend <- rev(colDend)
  }
  if (!is.null(revrows) && revrows) {
    rowDend <- rev(rowDend)
  }

  ## put color on the dendrograms
  if (!is.null(acolNclust)) {
    colDend <- colorDendrogram(colDend, acolNclust, ccolCol)
  }
  if (!is.null(browNclust)) {
    rowDend <- colorDendrogram(rowDend, browNclust, drowCol)
  }

  ## plot the heatmap again -- with colored dendrograms
  addargs <- as.list(substitute(list(...)))[-1L]
  addargs$Colv <- colDend
  addargs$Rowv <- rowDend
  hv <- do.call(andRstuff:::heatmap.2, addargs)

  ## silently return the heatmap object
  invisible(hv)
}

##' Plot a heatmap with colored row/col dendrograms and use seriation on the dendrograms
##'
##' This function is basically a call to heatmap.2.colden from this package
##' and, thus, it has the added functionality to color branches in the row
##' and/or col dendrograms.
##' Additionally the row and col dendrograms are reordered (without
##' changing the clustering!) see the seriation package on the details.
##' @param ... arguments passed through to heatmap.2
##' @param acolNclust number of clusters to color in the columns
##' @param browNclust number of clusters to color in the rows
##' @param ccolCol the colors to use in the column clusters
##' @param drowCol the colors to use in the row clusters
##' @param reduceIfNecessary boolean.  If TRUE the matrix will be reduced (i.e. rows/cols will be removed) until all distances are calculatable.
##' @param sermethod which method to use for the seriation ("GW", "OLO")
##' @param plots boolean (defaults to TRUE).  if FALSE do no plotting.
##' @return the heatmap.2 returned heatmap.  if plots==FALSE the a list with rowDendrogram and colDendrogram.
##' @author Andreas Leha
##' @name ser.heatmap.2.colden
##' @export
##' @examples
##' data(mtcars)
##' x  <- as.matrix(mtcars)
##' ser.heatmap.2.colden(sermethod="GW", browNclust=2, x=x)
ser.heatmap.2.colden <- function(...,
                                 acolNclust=NULL,
                                 browNclust=NULL,
                                 ccolCol,
                                 drowCol,
                                 reduceIfNecessary=TRUE,
                                 sermethod="OLO",
                                 plots=TRUE) {
  if (!require(seriation))
    stop("the 'seriation' package is needed for this function")

  addargs <- list(...)

  mat <- eval(if ("x" %in% names(addargs)) addargs[["x"]] else addargs[[1]])
  serRowv <- eval(if ("Rowv" %in% names(addargs)) addargs[["Rowv"]] else TRUE)
  serColv <- eval(if ("Colv" %in% names(addargs)) addargs[["Colv"]] else TRUE)
  serDistFun <- eval(if ("distfun" %in% names(addargs)) addargs[["distfun"]] else dist)

  rowDend <- serRowv
  colDend <- serColv

  if (is.logical(serRowv) && serRowv == TRUE) {
    ddd <- serDistFun(mat)
    if ((sum(is.na(ddd)) > 0) || (sum(is.infinite(ddd)) > 0)) {
      old_nrow <- nrow(mat)
      mat <- makeDistancable(mat, distFun = serDistFun)[[1]]
      new_nrow <- nrow(mat)
      if (new_nrow < old_nrow)
        warning(paste("removed", old_nrow-new_nrow, "rows to avoid NA dists"))
    }
    rowDend <- as.dendrogram(seriate(serDistFun(mat), method=sermethod)[[1]])
  } else {
    browNclust <- NULL
  }
  if (is.logical(serColv) && serColv == TRUE) {
    ddd <- serDistFun(t(mat))
    if ((sum(is.na(ddd)) > 0) || (sum(is.infinite(ddd)) > 0)) {
      old_ncol <- ncol(mat)
      mat <- t(makeDistancable(t(mat), distFun = serDistFun)[[1]])
      new_ncol <- ncol(mat)
      if (new_ncol < old_ncol)
        warning(paste("removed", old_ncol-new_ncol, "cols to avoid NA dists"))
    }
    colDend <- as.dendrogram(seriate(serDistFun(t(mat)), method=sermethod)[[1]])
  } else {
    acolNclust <- NULL
  }

  if (plots)
  {

    ## put the reduced mat into the arguments
    if ("x" %in% names(addargs)) addargs[["x"]] <- mat else addargs[[1]] <- mat

    addargs <- c(list(Colv=colDend, Rowv=rowDend), addargs)
    addargs <- addargs[!duplicated(names(addargs))]
    if (!is.null(acolNclust))
      addargs <- c(addargs, list(acolNclust=acolNclust))
    if (!is.null(browNclust))
      addargs <- c(addargs, list(browNclust=browNclust))
    if (!missing(ccolCol))
      addargs <- c(addargs, list(ccolCol=ccolCol))
    if (!missing(drowCol))
      addargs <- c(addargs, list(drowCol=drowCol))

    do.call(heatmap.2.colden, addargs)
  } else {
    list(rowDendrogram=rowDend, colDendrogram=colDend)
  }
}

##' Remove rows with many NAs to make sure that distances are all computable.
##'
##' This function removes rows from the input matrix one-by-one until
##' calling dist() on that matrix does not produce NAs any more.
##' NOTE: The result is not optimal (i.e. contains the maximum number of rows
##' possible).  The simple heuristic used here is, to remove in each step
##' the (first) row containing the maximal number of NAs.
##' @title Make Matrices Distancable
##' @param mat a matrix.  or anything that can be passed to dist()
##' @param distFun function.  which distance function to use
##' @return a (possibly) smaller version of mat.
##' @author Andreas Leha
##' @export
makeDistancable <- function(mat, distFun=dist) {
  to_keep <- rep(TRUE, nrow(mat))
  ddd <- distFun(mat[to_keep,])
  while((sum(is.na(ddd)) > 0) || (sum(is.infinite(ddd)) > 0)) {
    ## simple heuristic: remove row with most NAs
    num_NAs <- apply(mat, 1, function(x) sum(is.na(x)))
    ## remove the first row in case there are more
    idx_rem <- which((num_NAs == max(num_NAs[to_keep], na.rm=TRUE)) & to_keep)[1]

    to_keep[idx_rem] <- FALSE
    ddd <- distFun(mat[to_keep,])
  }
  return(list(mat=mat[to_keep,], to_keep=to_keep))
}


##' Remove rows with many NAs to make sure that distances are all computable.
##'
##' This function removes rows from the input matrix one-by-one until
##' calling dist() on that matrix does not produce NAs any more.
##' NOTE: The result is not optimal (i.e. contains the maximum number of rows
##' possible).  The simple heuristic used here is, to remove in each step
##' the (first) row containing the maximal number of NAs.
##' @title Make Matrices Distancable
##' @param mat a matrix.  or anything that can be passed to dist()
##' @param distFun function.  which distance function to use
##' @param serMethod character.  which seriation method to use.  see seriation package documentation
##' @return a (possibly) smaller version of mat.
##' @author Andreas Leha
##' @export
makeSerializable <- function(mat, distFun=dist, serMethod="OLO") {
  mat <- makeDistancable(mat, distFun = distFun)[[1]]

  to_keep <- rep(TRUE, nrow(mat))
  ddd <- distFun(mat[to_keep,])
  while (class(try(seriate(ddd, method=serMethod), silent=T))=="try-error") {
    ## simple heuristic: remove row with most NAs
    num_NAs <- apply(mat, 1, function(x) sum(is.na(x)))
    ## remove the first row in case there are more
    idx_rem <- which((num_NAs == max(num_NAs[to_keep], na.rm=TRUE)) & to_keep)[1]

    to_keep[idx_rem] <- FALSE
    cat(dim(mat[to_keep,]))
    ddd <- distFun(mat[to_keep,])
  }
  return(list(mat=mat[to_keep,], to_keep=to_keep))
}


##' use hclust to cluster the data
##'
##' @title cluster data using \code{hclust}
##' @param mat numeric matrix or data.frame.  the data.  will be clustered row-wise
##' @param n integer.  how many clusters to generate
##' @param minsize integer.  clusters with size smaller minsize will be considered outliers.
##' @return vector of lenght \code{nrow(mat)} containing the cluster assignment
##' @author Andreas Leha
##' @export
get_hclusts <- function(mat, n, minsize=3) {
  if (!require("coldendr"))
    stop("package 'coldendr' required")

  di <- dist(mat)
  hcl <- hclust(di)
  ddgr <- as.dendrogram(hcl)

  cls <- list()
  outlier <- rep(TRUE,n)
  usen <- n-1
  while(sum(!outlier) < n) {
    usen <- usen+1

    if (usen >= nrow(mat)/minsize)
      stop("data contains too many small clusters")

    h <- dendFindHeight(ddgr, usen)
    ddgr2 <- cut(ddgr, h)

    cls <- list()
    for (i in 1:usen) {
      cls[[as.character(i)]] <- unlist(dendrapply(ddgr2$lower[[i]], function(x) attributes(x)$label))
    }

    outlier <- unlist(lapply(cls, function(c) length(c) < minsize))
  }

  retcluster <- rep(NA, nrow(mat))
  for (i in (1:usen)[!outlier]) {
    retcluster[match(cls[[as.character(i)]], rownames(mat))] <- i
  }

  retcluster
}
